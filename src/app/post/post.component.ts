import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  @Input() titrePost: string;
  @Input() contenuPost: string;
  @Input() datePost: Date;

  @Input() loveits: number;

  constructor() { }

  ngOnInit() {
  }

  onAungmenter() {
    this.loveits ++;

  }

  onDiminuer() {
    this.loveits --;
  }

}
