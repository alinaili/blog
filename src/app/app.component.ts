import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'blog';

  //Array of Post
  posts = [
    {
        title: 'post 1',
        content: 'le premier post de blog le premier post de blog le premier post de blog le premier post de blog ',
        loveIts: 2,
        created_at: new Date()
    },
    {
      title: 'post 2',
        content: 'la deuxieme post de blog la deuxieme post de blog  de blog la deuxieme post de blog',
        loveIts: -1,
        created_at: new Date()
    },
    {
      title: 'post 3',
        content: 'la Troisieme post de blog la Troisieme post de blog la Troisieme post de blog la Troisieme post de blog',
        loveIts: 0,
        created_at: new Date()
    }
  ];
}
